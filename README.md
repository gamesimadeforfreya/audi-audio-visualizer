# Audi Audio Visualizer

I created this project for Audi's FourRingsChallenge.

Download the GodotEngine here: https://godotengine.org/ and then just import the project.godot file.


Wish me luck!


Thanks to Gonkee for the tutorial on audio visualization in the Godot Engine.
Tutorial : https://youtu.be/AwgSICbGxJM
