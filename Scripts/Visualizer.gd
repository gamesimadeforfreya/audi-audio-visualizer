extends Node2D

#
# Visualizer for Audi Contest 2020 by GamesIMadeForFreya
#

# Modified from Gonkee's audio visualiser tutorial for Godot 3.2: https://youtu.be/AwgSICbGxJM
# Thanks Gonkee!


onready var spectrum = AudioServer.get_bus_effect_instance(0, 0)
onready var ring_light : Light2D = $Light2D

export (NodePath) var audio_stream_player_path : NodePath
export var ring_index : int = 0

var definition = 20
var total_w = 400
var total_h = 200
var min_freq = 20
var max_freq = 20000

var max_db = 0
var min_db = -40

var accel = 20
var histogram = []

var audio_stream_player : AudioStreamPlayer
var ring : Node2D
var color_for_bars : Color


func _ready():
	color_for_bars = ring_light.get_color()
	audio_stream_player = get_node(audio_stream_player_path)
	ring = get_parent().get_parent()
	max_db += audio_stream_player.volume_db
	min_db += audio_stream_player.volume_db
	
	for _i in range(definition):
		histogram.append(0)

func _process(delta):
	var freq = min_freq
	var interval = (max_freq - min_freq) / definition
	
	for i in range(definition):
		
		var freqrange_low = float(freq - min_freq) / float(max_freq - min_freq)
		freqrange_low = freqrange_low * freqrange_low * freqrange_low * freqrange_low
		freqrange_low = lerp(min_freq, max_freq, freqrange_low)
		
		freq += interval
		
		var freqrange_high = float(freq - min_freq) / float(max_freq - min_freq)
		freqrange_high = freqrange_high * freqrange_high * freqrange_high * freqrange_high
		freqrange_high = lerp(min_freq, max_freq, freqrange_high)
		
		var mag = spectrum.get_magnitude_for_frequency_range(freqrange_low, freqrange_high)
		mag = linear2db(mag.length())
		mag = (mag - min_db) / (max_db - min_db)
		
		mag += 0.3 * (freq - min_freq) / (max_freq - min_freq)
		mag = clamp(mag, 0.05, 1)
		
		histogram[i] = lerp(histogram[i], mag, accel * delta)

	
	# Ring VFX
	ring.set_global_scale(Vector2(clamp(histogram[ring_index] + 0.95, 0, 1.125), clamp(histogram[ring_index] + 0.95, 0, 1.125)))
	rotation += (0.5 + histogram[ring_index]) * delta
	ring_light.set_energy(clamp(histogram[ring_index] + 1.25, 1.25, 1.5))
	
	update()

func _draw():
	# Bars VFX
	var angle = PI
	var angle_interval = 0.5 * PI / definition
	var radius = 480 / 2
	var length = 480 / 3.5
	
	for i in range(definition):
		var normal = Vector2(0, -1).rotated(angle)
		var start_pos = normal * radius
		var end_pos = normal * (radius + histogram[i] * length)
		draw_line(start_pos, end_pos, Color.white, 15.0, true)
		angle += angle_interval
